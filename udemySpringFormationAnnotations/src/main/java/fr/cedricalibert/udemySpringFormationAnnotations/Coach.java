package fr.cedricalibert.udemySpringFormationAnnotations;

public interface Coach {
	public String getDailyWorkout();
	
	public String getDailyFortune();
}
