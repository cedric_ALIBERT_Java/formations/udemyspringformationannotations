package fr.cedricalibert.udemySpringFormationAnnotations;

import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		return "C'est ton jour de chance";
	}

}
