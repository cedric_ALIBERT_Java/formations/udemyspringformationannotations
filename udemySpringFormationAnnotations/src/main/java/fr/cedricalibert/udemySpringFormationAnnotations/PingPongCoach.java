package fr.cedricalibert.udemySpringFormationAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class PingPongCoach implements Coach {

	@Autowired
	@Qualifier("fileFortuneService")
	private FortuneService fortuneService;
	
	@Override
	public String getDailyWorkout() {
		return "Fait des jongles";
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}

}
