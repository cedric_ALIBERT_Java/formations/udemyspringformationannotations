package fr.cedricalibert.udemySpringFormationAnnotations;

public class SadFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		return "Sad";
	}

}
