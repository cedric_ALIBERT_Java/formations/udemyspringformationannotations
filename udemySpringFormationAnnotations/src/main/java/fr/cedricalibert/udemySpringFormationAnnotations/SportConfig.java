package fr.cedricalibert.udemySpringFormationAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//@ComponentScan("fr.cedricalibert.udemySpringFormationAnnotations")
@PropertySource("classpath:sport.properties")
public class SportConfig {

	//define bean for SadFortuneService
	@Bean
	public FortuneService sadFortuneService() {
		return new SadFortuneService();
	}
	
	//define bean for SwimCoach AND inject Dependency
	@Bean
	public Coach swimCoach() {
		return new SwimCoach(this.sadFortuneService());
	}
	
}
