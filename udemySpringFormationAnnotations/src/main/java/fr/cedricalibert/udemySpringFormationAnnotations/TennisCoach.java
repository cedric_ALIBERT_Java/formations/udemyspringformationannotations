package fr.cedricalibert.udemySpringFormationAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

@Component
public class TennisCoach implements Coach {

	@Autowired
	@Qualifier("randomFortuneService")
	private FortuneService fortuneService;
	
	//@Autowired
	//public TennisCoach(FortuneService fortuneService) {
	//	this.fortuneService = fortuneService;
	//}
	
	public TennisCoach() {
		System.out.println(">> Inside default contructor : TennisCoach");
	}
	
	@Override
	public String getDailyWorkout() {
		return "Faire 3 match de 30 minutes !";
	}
	
	public FortuneService getFortuneService() {
		return fortuneService;
	}
	

	/*
	@Autowired
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println(">> Inside setFortuneService Method : TennisCoach");
		this.fortuneService = fortuneService;
	}
	*/

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}
	
	
	//add an init method
	@PostConstruct
	public void doMyStartupStuff() {
		System.out.println("TennisCoach: Inside method doMyStartupStuff");
	}
	
	//add a destroy method
	@PreDestroy
	public void doMyCleanUpStuff() {
		System.out.println("TennisCoach: Inside method doMyCleanUpStuff");
	}

}
